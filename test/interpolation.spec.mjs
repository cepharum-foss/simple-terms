import { describe, it } from "mocha";
import "should";

import { Interpolation, Functions } from "../lib/index.js";
const { interpolate, parse } = Interpolation;

describe( "String interpolation", () => {
	it( "is keeping string w/o any terms", () => {
		interpolate( "This is a string!" ).should.be.equal( "This is a string!" );
	} );

	it( "is keeping incompletely embedded term", () => {
		interpolate( "This is a {{ string!" ).should.be.equal( "This is a {{ string!" );
	} );

	it( "is keeping incompletely embedded term starting at beginning of string", () => {
		interpolate( "{{This is a string!" ).should.be.equal( "{{This is a string!" );
	} );

	it( "is keeping incompletely embedded term immediately following some proper term", () => {
		interpolate( "This is {{'a '}}{{ string!" ).should.be.equal( "This is a {{ string!" );
	} );

	it( "is keeping incompletely embedded term mediately following some proper term", () => {
		interpolate( "This is {{'a'}} {{ string!" ).should.be.equal( "This is a {{ string!" );
	} );

	it( "handles nested occurrences of opening marker", () => {
		interpolate( "This is {{'{{a}}'}} string!" ).should.be.equal( "This is {{a}} string!" );
	} );

	it( "handles C-style escapes for embedding occurrences of opening marker", () => {
		interpolate( "This is {{'\\{\\{a'}} string!" ).should.be.equal( "This is {{a string!" );
	} );

	it( "accepts custom strings for marking start and end of an embedded term", () => {
		interpolate( "This is <<'{{a'>> string!", {}, {}, { opener: "<<", closer: ">>" } ).should.be.equal( "This is {{a string!" );
	} );

	it( "handles multiple terms per string", () => {
		interpolate( "This is {{'a'}} {{'string'}}!" ).should.be.equal( "This is a string!" );
	} );

	it( "accepts custom functions to support in embedded terms", () => {
		interpolate( "This is {{foo('A')}} {{'str' + 'ing'}}!", {}, { foo: s => s.toLowerCase( s ) } ).should.be.equal( "This is a string!" );
	} );

	it( "rejects use of unknown functions", () => {
		( () => interpolate( "This is {{foo('A')}} {{'str' + 'ing'}}!" ) ).should.throw();
	} );

	it( "supports basic set of functions by default", () => {
		interpolate( "This is {{lowercase('A')}} {{'str' + 'ing'}}!" ).should.be.equal( "This is a string!" );
	} );

	it( "does not implicitly support that basic set of functions when providing empty pool custom functions", () => {
		( () => interpolate( "This is {{lowercase('A')}} {{'str' + 'ing'}}!", {}, {} ) ).should.throw();
	} );

	it( "does not implicitly support that basic set of functions when providing non-empty pool custom functions", () => {
		( () => interpolate( "This is {{lowercase('A')}} {{'str' + 'ing'}}!", {}, {} ) ).should.throw();
	} );

	it( "supports basic set of functions when providing them explicitly in a non-empty pool custom functions", () => {
		interpolate( "This is {{lowercase('A')}} {{'str' + 'ing'}}!", {}, { ...Functions } ).should.be.equal( "This is a string!" );
	} );

	it( "supports full scale of accepted term syntax", () => {
		interpolate( "This is {{test(6 * (2-3) > 5, (major.minor ), 'a')}} {{'str' + 'ing'}}!" ).should.be.equal( "This is a string!" );
	} );

	it( "unescapes code before compiling", () => {
		interpolate( "This is {{test(length('\\{') == 1, 'a', 'another')}} string!" ).should.be.equal( "This is a string!" );
	} );

	it( "supports reading data from separately provided data space", () => {
		interpolate( "This is {{ article }} {{ NOUN }}!", { article: "a", noun: "string" } ).should.be.equal( "This is a string!" );
	} );

	it( "supports reading data from separately provided data space with strict naming", () => {
		interpolate( "This is {{ Article }} {{ Noun }}!", { Article: "a", Noun: "string" }, null, { strictNaming: true } ).should.be.equal( "This is a string!" );
	} );

	describe( "provides function for parsing w/o actually evaluating which", () => {
		it( "is returning array of chunks by default", () => {
			parse( "" ).should.be.Array().which.is.empty();
			parse( " " ).should.be.Array().which.is.not.empty().and.deepEqual( [" "] );
		} );

		it( "is returning function on demand suitable for invoking with data and pool of functions to interpolate parsed string", () => {
			parse( "test" ).should.be.Array().which.is.not.empty();
			parse( "test", {}, {} ).should.be.Array().which.is.not.empty();
			parse( "test", {}, { asFunction: false } ).should.be.Array().which.is.not.empty();
			parse( "test", {}, { asFunction: true } ).should.be.Function().which.has.length( 2 );

			parse( "test", {}, { asFunction: true } )().should.be.String().which.is.equal( "test" );
			parse( "te {{ -1 * (3 + 6) }} st", {}, { asFunction: true } )().should.be.String().which.is.equal( "te -9 st" );
		} );

		it( "is returning provided string as-is on demand unless there is an embedded term", () => {
			parse( "test" ).should.be.Array().which.is.not.empty();
			parse( "test", {}, {} ).should.be.Array().which.is.not.empty();
			parse( "test", {}, { keepLiteralString: false } ).should.be.Array().which.is.not.empty();
			parse( "test", {}, { keepLiteralString: true } ).should.be.String().which.is.equal( "test" );
			parse( "test", {}, { keepLiteralString: true, asFunction: true } ).should.be.String().which.is.equal( "test" );
		} );
	} );
} );
