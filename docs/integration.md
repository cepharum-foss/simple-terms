---
prev: installation.md
next: api.md
---
# Integrating With Your Code

This package can be used in two ways:

1. for compiling string-based terms into functions

2. for interpolating terms embedded in a string by means of evaluating those terms and replacing them with their evaluation result in eventually returned string

## Compiling Terms

Let's start with a simple example:

```javascript
import { Compiler } from "simple-terms";

const term = Compiler.compile( "'hello ' + 'world!'" );

console.log( term() ); // showing: hello world!
```

`Compiler.compile()` is processing the string assuming it **completely consists of a term** to process.

:::tip
If the provided string is meant to contain literal parts in addition to the term to be processed, you should use [interpolation](#string-interpolation) instead.
:::

Well, computing literals doesn't really show off, so let's process some data:

```javascript
import { Compiler } from "simple-terms";

const term = Compiler.compile( "average( salaries ) > median( salaries ) * 1.3" );

let unbalanced = term( {
    salaries: [ 1000, 2000, 1800, 3600, 1900, 2650, 3180 ]
} );

console.log( unbalanced ); // showing: false

unbalanced = term( {
    salaries: [ 1100, 4540, 1320, 2030, 1280, 1410 ]
} );

console.log( unbalanced ); // showing: true
```

This time, the term is accessing some data provided on evaluating it. It is re-used as well using a different _data space_ on second evaluation.

In addition, there are functions used. By default, a [pool of functions](functions.md) is available. It is including functions `average()` and `median()` used in this example.

### Custom Functions

Of course, you can provide your own pool of functions:

```javascript
import { Compiler } from "simple-terms";

const term = Compiler.compile( "greet( user )", {
	greet: name => `Hello ${name}!`,
} );

console.log( term( { user: "John Doe" } ) ); // showing: "Hello John Doe!"
```

However, your provided pool of functions is replacing the default pool. Thus, the following example will fail:

```javascript
import { Compiler } from "simple-terms";

// this will throw for max isn't part of provided pool of available functions
const term = Compiler.compile( "max( 1, 2 )", {
	greet: name => `Hello ${name}!`,
} );

// this won't throw for default pool of functions is used
const term = Compiler.compile( "max( 1, 2 )" );
```

If you intend to extend the default pool of functions in scope of a term, you need include it with your custom pool of functions:

```javascript
import { Compiler, Functions } from "simple-terms";

const term = Compiler.compile( "max( 1, 2 )", {
	...Functions,
	greet: name => `Hello ${name}!`,
} );
```

### Caching

If you expect to compile a large amount of terms you might want to use a cache shared by all invocations of compiler:

```javascript
import { Compiler, Functions } from "simple-terms";

const first = Compiler.compile( "max( 1, 2 )" );
const second = Compiler.compile( "max( 1, 2 )" );

console.log( first === second ); // showing: false

const cache = new Map();
const first = Compiler.compile( "max( 1, 2 )", null, cache );
const second = Compiler.compile( "max( 1, 2 )", null, cache );

console.log( first === second ); // showing: true
```

Using a cache improves time spent on compiling terms. Memory consumption is reduced by re-using result of a previous compilation, as well.

However, there is a caveat when it comes to using a custom pool of function for the caching depends on that pool to succeed: 

```javascript
import { Compiler, Functions } from "simple-terms";

const cache = new Map();
const first = Compiler.compile( "max( 1, 2 )", { ...Functions }, cache );
const second = Compiler.compile( "max( 1, 2 )", { ...Functions }, cache );

console.log( first === second ); // showing: false

const pool = { ...Functions }
const first = Compiler.compile( "max( 1, 2 )", pool, cache );
const second = Compiler.compile( "max( 1, 2 )", pool, cache );

console.log( first === second ); // showing: true
```

In former two invocations separate pools containing same functions are provided. That's preventing caching from discovering those two instances to behave identical. In the latter two invocations a single pool has been prepared for re-use in either call for compiling a term which is helping the cache to succeed as desired. 

## String Interpolation

This package includes a helper for interpolating strings optionally containing embedded terms.

```javascript
import { Interpolation } from "simple-terms";

const string = "The result is {{ max( outer.inner, 3 ) * 4 }}!";
const result = Interpolation.interpolate( string, {
    outer: {
        inner: 6
    },
} );

console.log( result ); // showing "The result is 24!"
```

This helper is internally combining invocation of two separate functions `parse()` and `evaluate()` you might want to use directly to improve performance by parsing once but evaluating multiple times.

```javascript
import { Interpolation } from "simple-terms";

const string = "The result is {{ max( outer.inner, 3 ) * 4 }}!";
const parsed = Interpolation.parse( string );

let result = Interpolation.evaluate( parsed, { 
    outer: {
    	inner: 6
    },
} );

console.log( result ); // showing "The result is 24!"

result = Interpolation.evaluate( parsed, { 
    outer: {
    	inner: 2
    },
} );

console.log( result ); // showing "The result is 12!"
```

### Interpolation Options

By default, `parse()` is returning an array of chunks each representing either a literal part of provided string or a function resulting from compiling an encountered term. As an option, returning a function wrapping that invocation of `evaluate()` can be requested:

```javascript
import { Interpolation } from "simple-terms";

const string = "The result is {{ max( outer.inner, 3 ) * 4 }}!";
const fn = Interpolation.parse( string, null, {
    asFunction: true,
} );

let result = fn( { 
    outer: {
    	inner: 6
    },
} );

console.log( result ); // showing "The result is 24!"

result = fn( { 
    outer: {
    	inner: 2
    },
} );

console.log( result ); // showing "The result is 12!"
```

Furthermore, you might want to detect if there is a term embedded in provided string at all to prevent waste of time by collecting data for "evaluating" a simple string. By setting another option your code gets enabled to distinguish those cases, too:

```javascript
import { Interpolation } from "simple-terms";

let terms = Interpolation.parse( "The result is {{ max( outer.inner, 3 ) * 4 }}!", null, {
    keepLiteralString: true,
} );

console.log( typeof terms ); // showing: "object"

let terms = Interpolation.parse( "The result is {{ max( outer.inner, 3 ) * 4 }}!", null, {
    asFunction: true,
    keepLiteralString: true,
} );

console.log( typeof terms ); // showing: "function"

terms = Interpolation.parse( "The result is 24!", null, {
    keepLiteralString: true,
} );

console.log( typeof terms ); // showing: "string"

terms = Interpolation.parse( "The result is 24!", null, {
    asFunction: true,
    keepLiteralString: true,
} );

console.log( typeof terms ); // showing: "string"
```

As you can see, `keepLiteralString` is obeyed before `asFunction`.

Find additional options supported by interpolation in related [section of API reference](api.md#interpolation-parse).
