/**
 * Applies callback to provided string or any deeply nested string value in a
 * provided collection.
 *
 * @param {any} input string value to process or collection probably containing string values
 * @param {function(value:string):any} fn callback for processing any encountered string value
 * @returns {any} provided input with string value processed using callback
 */
export function processString( input, fn ) {
	return processScalar( input, value => ( typeof value === "string" ? fn( value ) : value ) );
}

/**
 * Applies callback to provided scalar value. If input is a collection, the
 * callback is applied to its enumerable scalar elements recursively.
 *
 * This function does not process nullish values but keeps them as-is.
 *
 * @param {any} input value to process or collection containing scalar values
 * @param {function(value:any):any} fn callback for processing any encountered scalar value
 * @returns {any} provided input with scalar value processed using callback
 */
export function processScalar( input, fn ) {
	switch ( typeof input ) {
		case "string" :
		case "number" :
		case "boolean" :
			return fn( input );

		case "object" :
			if ( input ) {
				if ( Array.isArray( input ) ) {
					const output = new Array( input.length );
					let write = 0;

					for ( const item of input ) {
						output[write++] = processScalar( item, fn );
					}

					return output;
				}

				if ( input instanceof Set ) {
					const output = new Array( input.size );
					let write = 0;

					for ( const item of input.values() ) {
						output[write++] = processScalar( item, fn );
					}

					return output;
				}

				if ( input instanceof Map ) {
					const output = new Map();

					for ( const [ key, value ] of input.entries() ) {
						output.set( key, processScalar( value, fn ) );
					}

					return output;
				}

				const output = {};

				for ( const name of Object.keys( input ) ) {
					output[name] = processScalar( input[name], fn );
				}

				return output;
			}

		// falls through
		default :
			return input;
	}
}
