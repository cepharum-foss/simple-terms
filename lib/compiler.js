import TermTokenizer from "./tokenizer.js";
import { Functions } from "./index.js";

const EmptyContext = Object.freeze( {} );


/**
 * Implements compiler converting sequence of tokens into function invocable for
 * processing term in context of some provided variable space.
 */
export default class TermCompiler {
	/**
	 * Compiles source code of a term into Javascript function evaluating it
	 * in context of data provided on invoking that function.
	 *
	 * @param {string} source source code of term to compile
	 * @param {FunctionsPool} functions set of functions available in term processing
	 * @param {TermsCache} cache refers to optional cache containing previously compiled terms
	 * @param {VariableQualifier} variableQualifier gets invoked to qualify variables accessed by term
	 * @param {boolean} strictDataNames if true, keywords naming properties of data to read from are kept as-is
	 * @returns {CompiledTerm} Javascript function implementing term
	 */
	static compile( source, functions = null, cache = null, variableQualifier = null, strictDataNames = false ) {
		const _functions = functions || Functions;
		let cachedResults;

		if ( cache ) {
			cachedResults = cache.get( source );

			if ( cachedResults ) {
				const numResults = cachedResults.length;

				for ( let i = 0; i < numResults; i++ ) {
					const cached = cachedResults[i];

					if ( cached.functions === _functions ) {
						return cached;
					}
				}
			}
		}

		const tokens = TermTokenizer.tokenizeString( source, true );
		const reduced = this.reduceTokens( tokens, _functions, variableQualifier, strictDataNames );
		const grouped = this.groupTokens( reduced );
		const code = this.compileTokens( grouped );

		const result = new Function( "functions", "data", `return ${code};` ).bind( EmptyContext, _functions );

		if ( cache ) {
			if ( cachedResults ) {
				cachedResults.push( result );
			} else {
				cache.set( source, [result] );
			}
		}

		Object.defineProperties( result, {
			dependsOn: { value: this.listReadVariables( reduced ) },
			functions: { value: _functions },
		} );

		return result;
	}

	/**
	 * Lists names of variables addressed in provided sequence of tagged tokens.
	 *
	 * @param {TaggedToken[]} taggedTokens lists tagged tokens
	 * @returns {Array<string[]>} lists paths of addressed variables
	 */
	static listReadVariables( taggedTokens ) {
		const paths = new Map();

		for ( let i = 0, numTokens = taggedTokens.length; i < numTokens; i++ ) {
			const taggedToken = taggedTokens[i];

			if ( taggedToken.variable ) {
				paths.set( taggedToken.path, true );
			}
		}

		const result = new Array( paths.size );
		let write = 0;

		for ( const path of paths.keys() ) {
			result[write++] = path;
		}

		return result;
	}

	/**
	 * Reduces sequences of tokens addressing nested variable into single token
	 * and tags most resulting tokens for simplified processing in next step.
	 *
	 * @param {Token[]} tokens sequence of tokens to reduce and tag
	 * @param {FunctionsPool} functions functions available in term processing
	 * @param {function(string[]):string[]} variableQualifier gets invoked to qualify variables accessed by term
	 * @param {boolean} strictDataNames if true, keywords naming properties of data to read from are kept as-is
	 * @returns {TaggedToken[]} reduced and tagged set of tokens
	 */
	static reduceTokens( tokens, functions, variableQualifier = null, strictDataNames = false ) {
		const numTokens = tokens.length;
		const reduced = new Array( numTokens );
		let write = 0;

		for ( let read = 0; read < numTokens; read++ ) {
			const token = tokens[read];

			switch ( token.type ) {
				case TermTokenizer.Types.WHITESPACE :
					// compiler expects sequence of tokens not including any whitespace token
					throw new Error( `unexpected whitespace token at ${token.offset}` );

				case TermTokenizer.Types.DEREF_OPERATOR :
					throw new Error( `unexpected dereferencing operator at ${token.offset}` );

				case TermTokenizer.Types.UNARY_LOGIC_OPERATOR :
					if ( token.value === "!" ) {
						// logical negation of term -> detect and handle multiple occurrences
						let advance;

						for ( advance = read + 1; advance < numTokens; advance++ ) {
							if ( tokens[advance].type !== TermTokenizer.Types.UNARY_LOGIC_OPERATOR ) {
								break;
							}
						}

						if ( ( advance - read ) % 2 === 1 ) {
							// odd number of unary operators
							// -> consume first occurrence as-is below, but omit
							//    all additional occurrences
							read = advance - 1;
						}
					}

				// falls through
				case TermTokenizer.Types.BINARY_COMPARISON_OPERATOR :
				case TermTokenizer.Types.BINARY_ARITHMETIC_OPERATOR :
				case TermTokenizer.Types.BINARY_LOGIC_OPERATOR :
					token.operator = true;
					token.operand = false;
					token.path = null;
					token.literal = false;
					token.variable = false;
					token.function = false;

					reduced[write++] = token;
					break;

				case TermTokenizer.Types.LITERAL_INTEGER :
				case TermTokenizer.Types.LITERAL_FLOAT :
					token.value = token.value.replace( /^([+-]?)0+(\d)/, "$1$2" );

				// falls through
				case TermTokenizer.Types.LITERAL_STRING :
					token.operator = false;
					token.operand = true;
					token.path = null;
					token.literal = true;
					token.variable = false;
					token.function = false;

					reduced[write++] = token;
					break;

				case TermTokenizer.Types.KEYWORD : {
					token.operator = false;
					token.operand = true;

					let wantDeref, j;

					for ( j = read + 1, wantDeref = true; j < numTokens; j++, wantDeref = !wantDeref ) {
						const succeedingToken = tokens[j];
						if ( !succeedingToken || succeedingToken.type !== ( wantDeref ? TermTokenizer.Types.DEREF_OPERATOR : TermTokenizer.Types.KEYWORD ) ) {
							break;
						}
					}

					if ( !wantDeref ) {
						// fragment ended with deref operator -> need another keyword then
						throw new Error( `unexpected end of identifier at ${( tokens[j] || { offset: "end of code" } ).offset}` );
					}

					const size = j - read;

					if ( size > 1 ) {
						const numLevels = Math.ceil( size / 2 );
						const path = new Array( numLevels );

						for ( let r = read, w = 0; w < numLevels; r += 2, w++ ) {
							path[w] = strictDataNames ? tokens[r].value : tokens[r].value.toLowerCase();
						}

						const combined = tokens.slice( read, j ).map( t => t.value ).join( "" );
						token.value = strictDataNames ? combined : combined.toLowerCase();
						token.path = path;

						token.literal = false;
						token.variable = true;
						token.function = false;
					} else {
						switch ( token.value.toLowerCase() ) {
							case "true" :
							case "false" :
							case "null" :
								token.value = token.value.toLowerCase();
								token.path = null;
								token.literal = true;
								token.variable = false;
								token.function = false;
								break;

							default : {
								const nextToken = tokens[j];
								const isInvoking = Boolean( nextToken && nextToken.type === TermTokenizer.Types.PARENTHESIS && nextToken.value === "(" );

								if ( isInvoking ) {
									token.value = token.value.toLowerCase();

									if ( !functions.hasOwnProperty( token.value ) || typeof functions[token.value] !== "function" ) {
										throw new Error( `invocation of unknown function at ${token.offset}` );
									}
								} else if ( !strictDataNames ) {
									token.value = token.value.toLowerCase();
								}

								token.path = [token.value];
								token.literal = false;
								token.variable = !isInvoking;
								token.function = isInvoking;
							}
						}
					}

					if ( token.variable && typeof variableQualifier === "function" ) {
						token.path = variableQualifier( token.path );
					}

					reduced[write++] = token;
					read = j - 1;

					break;
				}

				default :
					token.operator = false;
					token.operand = false;
					token.path = null;
					token.literal = false;
					token.variable = false;
					token.function = false;

					reduced[write++] = token;
			}
		}

		reduced.splice( write );

		return reduced;
	}

	/**
	 * Converts sequence of tagged tokens into multi-level hierarchy of grouped
	 * tokens.
	 *
	 * @param {TaggedToken[]} tokens sequence of reduced and tagged tokens
	 * @returns {GroupedToken[]} hierarchy of grouped tokens
	 */
	static groupTokens( tokens ) {
		const stack = [{ group: "term", tokens: [] }];

		for ( let i = 0, numTokens = tokens.length; i < numTokens; i++ ) {
			const token = tokens[i];

			switch ( token.type ) {
				case TermTokenizer.Types.PARENTHESIS :
					switch ( token.value ) {
						case "(" :
							if ( stack[0].group === "function" && stack[0].tokens.length === 0 ) {
								break;
							}

							stack.unshift( {
								group: "term",
								tokens: [],
								offset: token.offset,
							} );
							break;

						case ")" : {
							if ( stack.length <= 1 ) {
								throw new Error( `unexpected closing parenthesis at ${token.offset}` );
							}

							const group = stack.shift();

							switch ( group.group ) {
								case "term" :
									switch ( group.tokens.length ) {
										case 0 :
											throw new Error( `invalid empty pair of parentheses at ${group.offset}` );

										case 1 :
											// remove unnecessary pair of parentheses
											stack[0].tokens.push( group.tokens[0] );
											break;

										default :
											stack[0].tokens.push( this.normalizeTermGroup( group ) );
									}
									break;

								case "function" :
									stack[0].tokens.push( this.normalizeFunctionGroup( group ) );
									break;

								default :
									throw new Error( `unexpected end of block at ${token.offset} started at ${stack[0].tokens[0].offset}` );
							}

							break;
						}
					}
					break;

				case TermTokenizer.Types.UNARY_LOGIC_OPERATOR :
					stack.unshift( {
						group: "unary",
						tokens: [],
						offset: token.offset,
					} );
					break;

				case TermTokenizer.Types.KEYWORD :
					if ( token.function ) {
						stack.unshift( {
							group: "function",
							name: token.value,
							tokens: [],
							offset: token.offset,
						} );
						break;
					}

				// falls through
				default :
					stack[0].tokens.push( token );
			}


			while ( stack[0].group === "unary" && stack[0].tokens.length > 0 ) {
				if ( stack[0].tokens.length > 1 ) {
					throw new TypeError( "unexpected multiple operands to unary operator" );
				}

				// got required operand to unary operator -> auto-close its group
				const unaryGroup = stack.shift();

				const unaryOperand = unaryGroup.tokens[0];
				const wrappingGroup = {
					tokens: [unaryOperand],
					offset: unaryOperand.offset,
					negated: unaryGroup.negated,
				};

				switch ( unaryOperand.group ) {
					case "function" : {
						unaryOperand.negated = true;
						stack[0].tokens.push( unaryOperand );
						break;
					}

					case "term" :
					default : {
						const normalized = this.normalizeTermGroup( wrappingGroup );
						normalized.negated = true;
						stack[0].tokens.push( normalized );
						break;
					}
				}
			}
		}

		if ( stack.length > 1 ) {
			throw new Error( `unexpected end of code while processing block started at ${stack[0].offset}` );
		}

		return [this.normalizeTermGroup( stack[0] )];
	}

	/**
	 * Dumps provided group of terms stdout.
	 *
	 * @param {GroupedToken} tree group token representing hierarchy of token groups
	 * @param {string} indent additional indentation, mostly for internal use, only, omit on invocation
	 * @returns {void}
	 */
	static dump( tree, indent = "" ) {
		switch ( tree.group ) {
			case "term" :
				console.log( `${indent}*${tree.negated ? " negated" : ""} ${tree.group} {` ); // eslint-disable-line no-console
				for ( let i = 0; i < tree.tokens.length; i++ ) {
					this.dump( tree.tokens[i], indent + "  " );
				}
				console.log( `${indent}  }` ); // eslint-disable-line no-console
				break;

			case "function" :
				console.log( `${indent}*${tree.negated ? " negated" : ""} function ${tree.name} ${tree.args.length ? "w/ args:" : "w/o args"}` ); // eslint-disable-line no-console
				for ( let i = 0; i < tree.args.length; i++ ) {
					this.dump( tree.args[i], indent + "  " );
				}
				console.log( `${indent}  }` ); // eslint-disable-line no-console
				break;

			default :
				console.log( `${indent}*${tree.negated ? " negated" : ""}`, tree.type ); // eslint-disable-line no-console
		}
	}

	/**
	 * Validates if provided sequence of tokens is a valid sequence for a term.
	 *
	 * @param {RawGroup} group group of raw tokens amounting to single term
	 * @param {boolean} negateResult true if normalized group should be negated, too
	 * @returns {TermGroup} normalized group describing single term
	 * @throws Error on invalid token structure
	 */
	static normalizeTermGroup( group, negateResult = false ) {
		const { tokens, offset, negated = false } = group;
		let numTokens = tokens.length;

		if ( !numTokens ) {
			throw new Error( "invalid empty term" );
		}

		switch ( tokens[0].type ) {
			case TermTokenizer.Types.UNARY_LOGIC_OPERATOR :
				if ( numTokens < 2 ) {
					throw new Error( "invalid unary operator w/o operand" );
				}

				if ( numTokens > 2 ) {
					throw new Error( "invalid unary operator w/ multiple operands" );
				}

				if ( !tokens[1].operand ) {
					throw new Error( `expecting operand after unary operator at ${tokens[1].offset}, but found ${tokens[1].group || tokens[1].type.toString()}` ); // eslint-disable-line max-len
				}

				break;

			default :
				for ( let i = 0; i < numTokens; i++ ) {
					const token = tokens[i];

					switch ( i % 2 ) {
						case 0 :
							// expecting operand
							if ( !token.operand ) {
								throw new Error( `expecting operand in term at ${token.offset} but found ${token.type.toString()}` );
							}
							break;

						case 1 :
							// expecting operator
							if ( token.operator ) {
								if ( token.type === TermTokenizer.Types.UNARY_LOGIC_OPERATOR ) {
									throw new Error( `unexpected unary operator ${token.value} in term at ${token.offset}` );
								}

								break;
							}

							if ( token.operand && ( token.type === TermTokenizer.Types.LITERAL_INTEGER || token.type === TermTokenizer.Types.LITERAL_FLOAT ) && "+-".indexOf( token.value.charAt( 0 ) ) > -1 ) {
								// misinterpreted binary operator as sign of succeeding integer before
								// -> fix here
								const operator = token.value.charAt( 0 );

								token.offset++;
								token.value = token.value.slice( 1 );

								tokens.splice( i, 0, {
									type: TermTokenizer.Types.BINARY_ARITHMETIC_OPERATOR,
									value: operator,
									offset: token.offset,
									operand: false,
									operator: true,
									path: null,
									literal: false,
									variable: false,
									function: false,
								} );

								numTokens++;
								i++;

								break;
							}

							throw new Error( `expecting operator in term at ${token.offset} but found ${token.group || token.type.toString()}` );
					}
				}
		}

		return {
			group: "term",
			tokens,
			offset,
			operand: true,
			operator: false,
			literal: false,
			path: null,
			variable: false,
			function: false,
			negated: negateResult ? !negated : negated,
		};
	}

	/**
	 * Validates and normalizes a group of raw if provided sequence of tokens is a valid sequence for a
	 * function invocation.
	 *
	 * @param {RawGroup} group group of raw tokens amounting to invocation of a function
	 * @param {boolean} negateResult true if normalized group should be negated, too
	 * @returns {FunctionGroup} normalized group of tokens describing invocation of a function
	 * @throws Error on invalid token structure
	 */
	static normalizeFunctionGroup( group, negateResult = false ) {
		const { name, tokens, offset, negated = false } = group;
		const args = [];

		// split provided sequence of tokens into sequences of tokens per
		// argument to function
		const numTokens = tokens.length;
		let start = 0;

		for ( let i = 0; i < numTokens; i++ ) {
			if ( tokens[i].type === TermTokenizer.Types.COMMA ) {
				if ( i > start ) {
					args.push( tokens.slice( start, i ) );
				} else {
					args.push( null );
				}

				start = i + 1;
			}
		}

		if ( numTokens > start ) {
			args.push( tokens.slice( start ) );
		}


		// ignore trailing commata
		let end = args.length;
		for ( ; end >= 0; end-- ) {
			if ( args[end] != null ) {
				break;
			}
		}

		args.splice( end + 1 );


		// replace further empty arguments w/ literal value `null`, normalize
		// all provided arguments as term
		for ( let i = 0, numArgs = args.length; i < numArgs; i++ ) {
			const arg = args[i];

			if ( arg == null ) {
				args[i] = {
					type: TermTokenizer.Types.KEYWORD,
					value: "null",
					operand: true,
					operator: false,
					literal: true,
					path: null,
					variable: false,
					function: false,
				};
			} else {
				args[i] = this.normalizeTermGroup( {
					group: "term",
					tokens: arg,
					offset: arg[0].offset,
				} );
			}
		}


		return {
			group: "function",
			name,
			args,
			offset,
			operand: true,
			operator: false,
			literal: false,
			path: null,
			variable: false,
			function: false, // seems odd, but this tag is used to mark raw token naming some function
			negated: negateResult ? !negated : negated,
		};
	}

	/**
	 * Compiles sequence of reduced, tagged and grouped tokens into source code
	 * of a Javascript function's body.
	 *
	 * @param {GroupedToken[]} tokens reduced, tagged and grouped set of tokens
	 * @returns {string} source code of Javascript function's body processing term
	 */
	static compileTokens( tokens ) {
		const numTokens = tokens.length;
		const code = new Array( numTokens );

		for ( let i = 0; i < numTokens; i++ ) {
			const token = tokens[i];

			switch ( token.group ) {
				case "term" :
					code[i] = ( token.negated ? "!" : "" ) + `(${this.compileTokens( token.tokens )})`;
					break;

				case "function" : {
					const numArgs = token.args.length;
					const args = new Array( numArgs );

					for ( let j = 0; j < numArgs; j++ ) {
						const source = token.args[j];

						args[j] = this.compileTokens( source.group ? source.tokens : [source] );
					}

					code[i] = ( token.negated ? "!" : "" ) + `(functions["${token.name.replace( /"/g, '\\"' )}"](${args.join( "," )}))`;
					break;
				}

				default :
					switch ( token.type ) {
						case TermTokenizer.Types.BINARY_COMPARISON_OPERATOR :
							if ( token.value === "<>" ) {
								code[i] = "!=";
							} else if ( token.value === "=" ) {
								code[i] = "==";
							} else {
								code[i] = token.value;
							}
							break;

						case TermTokenizer.Types.KEYWORD :
							if ( token.literal ) {
								code[i] = token.value;
							} else {
								code[i] = "data";

								for ( let j = 0, segments = token.path, numSegments = segments.length; j < numSegments; j++ ) {
									if ( j === numSegments - 1 ) {
										code[i] = `(${code[i]}["${segments[j].replace( /"/g, '\\"' )}"])`;
									} else {
										code[i] = `(${code[i]}["${segments[j].replace( /"/g, '\\"' )}"]||{})`;
									}
								}
							}
							break;

						case TermTokenizer.Types.LITERAL_INTEGER :
						case TermTokenizer.Types.LITERAL_FLOAT :
							code[i] = `(${token.value})`;
							break;

						default :
							code[i] = token.value;
					}
			}
		}

		return code.join( "" );
	}
}


/**
 * @typedef {Token} TaggedToken
 * @property {boolean} operator marks if token represents an operator
 * @property {boolean} operand marks if token represents an operand
 * @property {?string[]} path provides segments in a multi-level reference to variable
 * @property {boolean} literal marks if token represents literal value
 * @property {boolean} variable marks if token is addressing variable in data provided on term processing
 * @property {boolean} function marks if token is addressing function for invocation
 */

/**
 * @typedef {TermGroup|FunctionGroup} TokenGroup
 */

/**
 * @typedef {object} RawGroup
 * @property {string} group indicates type of group, either "term" or "function"
 * @property {GroupedToken[]} tokens sequence of tokens amounting to group
 * @property {int} [offset] index into source code of first character in this group
 * @property {string} [name] name of a function in a group going to describe function invocation
 */

/**
 * @typedef {TaggedToken} TermGroup
 * @property {string} group marks current group as such, using value "term"
 * @property {GroupedToken[]} tokens sequence of tokens amounting to term
 * @property {int} offset index into source code of first character in this group
 */

/**
 * @typedef {TaggedToken} FunctionGroup
 * @property {string} group marks current group as such, using value "function"
 * @property {string} name name of function to invoke
 * @property {(TaggedToken|TermGroup)[]} args list of arguments to provide on invocation of function
 * @property {int} offset index into source code of first character in this group
 */

/**
 * @typedef {TaggedToken|TokenGroup} GroupedToken
 */
