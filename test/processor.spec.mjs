import { describe, it } from "mocha";
import Should from "should";

import { Processor } from "../lib/index.js";

describe( "Term Processor", () => {
	it( "is available", () => {
		Should.exist( Processor );
	} );

	it( "compiles term to invocable function", () => {
		new Processor( "ROUND( myvar.float, 1 ) == 0.5" )
			.evaluate( {
				myvar: {
					integer: 1,
					float: 0.53,
				}
			} )
			.should.be.true();
	} );

	it( "compiles term containing string literal with escaped quotes to invocable function", () => {
		new Processor( `'0123\\'56789' == "0123'56789"` )
			.evaluate( {} )
			.should.be.true();

		new Processor( `length( '0123\\'56789' ) == 10` )
			.evaluate( {} )
			.should.be.true();

		new Processor( `"0123\\"56789" == '0123"56789'` )
			.evaluate( {} )
			.should.be.true();

		new Processor( `length( "0123\\"56789" ) == 10` )
			.evaluate( {} )
			.should.be.true();

		new Processor( `length( "0123\\'56789" ) == 10` )
			.evaluate( {} )
			.should.be.true();

		new Processor( `length( '0123\\"56789' ) == 10` )
			.evaluate( {} )
			.should.be.true();
	} );

	it( "support negating result of invoked function", () => {
		new Processor( "empty( value )" )
			.evaluate( {
				value: "",
			} )
			.should.be.true();

		new Processor( "!empty( value )" )
			.evaluate( {
				value: "",
			} )
			.should.be.false();
	} );
} );
