import TermCompiler from "./compiler.js";
import * as TermFunctions from "./functions.js";


/**
 * Implements handling of computable terms.
 */
export default class TermProcessor {
	/**
	 * @param {string} source source code of term
	 * @param {object<string,function>} customFunctions map of custom functions to support in term
	 * @param {Map<string,function>} compilerCache refers to optional cache containing previously compiled terms
	 * @param {function(string[]):string[]} variableQualifier gets invoked to qualify variables accessed by term
	 */
	constructor( source, customFunctions = {}, compilerCache = null, variableQualifier = null ) {
		const functions = Object.assign( {}, TermFunctions, customFunctions );
		const compiled = TermCompiler.compile( source, functions, compilerCache, variableQualifier );

		Object.defineProperties( this, {
			/**
			 * Provides code of term as provided on term creation.
			 *
			 * @name TermProcessor#source
			 * @property {string}
			 * @readonly
			 */
			source: { value: source },

			/**
			 * Evaluates term in context of a provided variable space.
			 *
			 * @name TermProcessor#code
			 * @property {function(data:object):*}
			 * @readonly
			 */
			code: { value: compiled },

			/**
			 * Lists paths of variables this term depends on.
			 *
			 * @name TermProcessor#dependsOn
			 * @property {Array<string[]>}
			 * @readonly
			 */
			dependsOn: { value: compiled.dependsOn },

			/**
			 * Exposes qualified set of functions to be available in context of
			 * current term.
			 *
			 * @name TermProcessor#functions
			 * @property {object<string,function>}
			 * @readonly
			 */
			functions: { value: Object.freeze( functions ) },
		} );
	}

	/**
	 * Evaluates compiled term in context of provided data.
	 *
	 * @param {object<string,*>} data variable space exposing data available to evaluated term
	 * @returns {*} result of evaluating term
	 */
	evaluate( data = {} ) {
		return this.code( data, this.functions );
	}
}
