---
sidebarDepth: 0
prev: api.md
next: functions.md
---
# Language Tutorial

:::warning Current State, only  
This document illustrates current state of development. It isn't meant to limit future revisions of supported terms' syntax. If you need additional features feel free to open an [issue](https://gitlab.com/cepharum-foss/simple-terms/-/issues) or provide a pull request.  
:::

## Terms, Only!

Let's start with the most obvious limitation: **simple-terms** is processing terms, only. There is no support for any kind of statement. There are no loops, no conditionals, no function definitions or variable declarations.

This is by intention for terms are meant to derive information from existing data, only. They aren't designed to adjust any existing data permanently or do some heavy processing. You should create your own pool of functions if you need to do something like that.

This implies: terms always return a single result.

## Whitespace

### Supported

All kinds of whitespace are supported between operands and operators.

### Not Supported

Vertical whitespace in string literals is not supported. Thus, you can't use line breaks there.

## Literals

### Supported

Most scalar literals are supported. This includes numbers in decimal form:

```javascript
12.45
```

```javascript
+40000
```

strings:

```javascript
"double quoted"
```

```javascript
'single quoted'
```

booleans:

```javascript
true
```

```javascript
false
```

and

```javascript
null
```

#### Escape Sequences

In string literals, C-style escape sequences are supported. These are kept as-is by _simple-terms_ parser for the resulting Javascript function is going to contain the same literal string.

### Not Supported

First of all, non-scalar literals are not supported. However, there are some [functions](functions.md) available for processing non-scalar data.

```javascript
[ 1, 2, 3 ]
```

```javascript
{ foo: "bar", baz: "bam" }
```

:::warning How to create ...  
... an [array](./functions.md#array) like the one given above:

```javascript
array( 1, 2, 3 )
```

... an [object](./functions.md#dict) like the one given before:

```javascript
dict( "foo", "bar", "baz", "bam" )
```
:::

Regarding scalar literals, numbers must not be written in scientific notation:

```javascript
1.3e+05
```

They must not be given with any radix other than 10:

```javascript
0xfffe
```

Last but not least, you must not use

```javascript
undefined
```

or

```javascript
NaN
```

or

```javascript
Infinity
```

## Arithmetic Expressions

### Supported

You can use binary arithmetic expressions like

```javascript
4 + 20.5
```

or

```javascript
( 4 * 20.5 ) - 12 / 7
```

Supported operators are `+`, `-`, `*` and `/`. 

Order of processing depends on Javascript engine used eventually for evaluating such expressions.

:::warning Beware of Type Coercion   
Basically, _simple-terms_ qualifies a string containing a term to be code of Javascript function. Thus, arithmetic expressions imply [type coercion](https://developer.mozilla.org/en-US/docs/Glossary/Type_coercion) whenever required.

Because of that, adding strings results in their concatenation. This example results in string containing `hello world`:

```javascript
"hello " + 'world'
```

However, due to type coercion the same applies to adding a string and e.g. a number:

```javascript
"1" + 5
```

This results in string `15`.

There are some [default functions](functions.md#type-casting) available for casting types explicitly.  
:::

### Not Supported

You **can not** use shorthand negations e.g. before opening parenthesis of an expression. The following example **does not work** because of the leading dash:

```javascript
-( 4 * 20.5 )
```

## Bit-wise Logical Expressions

### Supported

Operators `@` and `|` are supported for bit-wise logical operations.

```javascript
1234 & 8
```

:::warning Type Coercion  
See the note on [type coercion](https://developer.mozilla.org/en-US/docs/Glossary/Type_coercion) in section about [arithmetic expressions](#arithmetic-expressions).
:::

### Not Supported

The two's complement unary operator `~` is not supported:

```javascript
~1234
```

## Logical Expressions

### Supported

Logical binary operators `&&` and `||` are supported. 

```javascript
( accepted && "alternative" ) || "some fallback"
```

Their semantics is identical to those supported by Javascript, and so you can use them for declaring fallbacks or alternatives as demonstrated above.

In addition, unary logical negation `!` is supported either:

```javascript
!done
```

For being recognized as an unary operator, it may appear right before an opening parenthesis, too:

```javascript
!(done && idle)
```

:::warning Type Coercion  
See the note on [type coercion](https://developer.mozilla.org/en-US/docs/Glossary/Type_coercion) in section about [arithmetic expressions](#arithmetic-expressions).
:::

## Comparison Expressions

### Supported

Binary comparison operators `<`, `<=`, `==`, `<>`, `>=` and `>` are supported:

```javascript
5 >= 12
```

:::warning Type Coercion  
See the note on [type coercion](https://developer.mozilla.org/en-US/docs/Glossary/Type_coercion) in section about [arithmetic expressions](#arithmetic-expressions).
:::

### Not Supported

The parser fails to handle `!=`, so this is not supported. Use `<>` instead.

In addition, type-safe comparisons using `===` are not supported either.

## Keywords

### Supported

Any consecutive sequence of latin letters, digits, underscores and `$` is considered a _keyword_. Keywords must not start with a digit, though.

A keyword can be used in one of two cases:

1. When a keyword is followed by an opening parenthesis, it is considered to select a function to be invoked.

   ```javascript
   max( 1, 2, 3 )
   ```
   
   Function invocations may be nested:

   ```javascript
   max( 1, 2, min( array( 6, 3, 8 ) ) )
   ```
   
2. Every other keyword is considered to address a property of data space provided on evaluating the term. 

   ```javascript
   first + second > third
   ```

   Such a keyword may be followed by a dot operator `.` and another keyword for descending into the hierarchy of data space.

   ```javascript
   some.property.value
   ```
   
   Using this notation does not throw like Javascript when any of the addressed properties along the path is missing or _falsy_. The resulting value is always _nullish_ in these cases.

   :::tip What is the value?  
   A keyword is representing a value read from a _data space_ provided on evaluating term.  
   :::

### Not Supported

By design, you can not invoke methods of an object available in data space by starting complex keyword using dot notation and have the last part followed by an opening parenthesis.

```javascript
some.method()
```

Supported functions must be provided separately from data space at compile-time. This is intended behaviour to improve security.
