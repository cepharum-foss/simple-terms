import Tokenizer from "./tokenizer.js";
import Compiler from "./compiler.js";
import Processor from "./processor.js";
import * as FunctionsPool from "./functions.js";
import * as Interpolation from "./interpolate.js";

const Functions = Object.freeze( { ...FunctionsPool } );

export {
	Tokenizer,
	Compiler,
	Processor,
	Functions,
	Interpolation,
};
