import { Compiler, Functions } from "./index.js";

/**
 * @typedef {object} InterpolationOptions
 * @property {string} [opener] string indicating start of embedded term, defaults to "{{"
 * @property {string} [closer] string indicating end of embedded term, defaults to "}}"
 * @property {Map<string, Function>} [cache] collects compiled terms and their compilation result for re-use
 * @property {function(string[]):string[]} [qualifier] gets invoked to qualify variable references found in terms
 * @property {boolean} asFunction if true, parse() will return a Function to invoke with data and functions to resolve embedded terms
 * @property {boolean} keepLiteralString if true, parse() will return provided string as-is if there is no embedded term
 * @property {boolean} strictNaming if true, keywords addressing variables to read are not converted to lowercase but kept as-is
 */

const Escapes = {
	r: "\r",
	n: "\n",
	t: "\t",
};

const unescape = string => string.replace( /\\(.)/g, ( _, code ) => Escapes[code] || code );

/**
 * Processes terms embedded in provided string.
 *
 * @param {string} string string optionally consisting of markers containing terms to process
 * @param {object} data provides data space available for reading in terms
 * @param {FunctionsPool} functions pool of functions available for invocation in terms
 * @param {InterpolationOptions} options customizes interpolation process
 * @returns {string} interpolated string
 */
export function interpolate( string, data, functions, options = {} ) {
	return parse( string, functions, { ...options, keepLiteralString: false, asFunction: true } )( data );
}

/**
 * Splits provided string into sequence of chunks each representing either some
 * embedded term compiled for evaluation or some literal string.
 *
 * @param {string} string string to parse
 * @param {FunctionsPool} functions pool of functions available for invoking in terms
 * @param {InterpolationOptions} options customizes interpolation process
 * @returns {InterpolationParserResult} list of chunks, literal string as-is or evaluation function depending on options
 */
export function parse( string, functions = undefined, options = {} ) {
	const length = string.length;
	const { opener = "{{", closer = "}}" } = options || {};
	const openerLength = opener.length;
	const closerLength = closer.length;
	const chunks = [];
	let depth = 0;
	let cursor = 0;
	let start = 0;

	while ( cursor < length ) {
		const nextOpener = string.indexOf( opener, cursor );
		const nextCloser = depth > 0 ? string.indexOf( closer, cursor ) : -1;

		let hasOpener = nextOpener > -1;
		let hasCloser = nextCloser > -1;

		if ( hasOpener && hasCloser ) {
			if ( nextOpener < nextCloser ) {
				hasCloser = false;
			} else {
				hasOpener = false;
			}
		}

		if ( hasOpener ) {
			if ( depth < 1 ) {
				chunks.push( unescape( string.substring( start, nextOpener ) ) );
				start = cursor = nextOpener + openerLength;
			} else {
				cursor = nextOpener + closerLength;
			}

			depth++;
		} else if ( hasCloser ) {
			depth--;

			if ( depth < 1 ) {
				chunks.push( Compiler.compile(
					unescape( string.substring( start, nextCloser ) ),
					functions || Functions,
					options.cache,
					options.qualifier,
					options.strictNaming
				) );

				start = cursor = nextCloser + closerLength;
			} else {
				cursor = nextCloser + closerLength;
			}
		} else {
			const index = depth > 0 ? Math.max( 0, start - openerLength ) : start;

			chunks.push( unescape( string.substring( index ) ) );
			break;
		}
	}

	if ( options.keepLiteralString && ( !chunks.length || ( chunks.length === 1 && typeof chunks[0] === "string" ) ) ) {
		return string;
	}

	if ( options.asFunction ) {
		return ( d, f ) => evaluate( chunks, d, f );
	}

	return chunks;
}

/**
 * Evaluates list of chunks resulting from parsing a string replacing found
 * terms with either term's result.
 *
 * @param {Array<(string|Compiler)>} chunks list of chunks created with parse()
 * @param {object} data data space available for addressing in terms
 * @returns {string} string resulting from evaluating all terms in list prior to joining chunks again
 */
export function evaluate( chunks, data ) {
	if ( !Array.isArray( chunks ) ) {
		return "";
	}

	const count = chunks.length;

	if ( count === 1 && typeof chunks[0] === "string" ) {
		return chunks[0];
	}

	const results = new Array( count );

	for ( let i = 0; i < count; i++ ) {
		const chunk = chunks[i];

		if ( chunk instanceof Function ) {
			results[i] = chunk( data || {} );
		} else {
			results[i] = String( chunk );
		}
	}

	return results.join( "" );
}
