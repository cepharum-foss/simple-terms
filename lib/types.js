/**
 * Lists supported types of tokens.
 *
 * @name TokenTypes
 * @type {object<string,Symbol>}
 */
export const TokenTypes = Object.freeze( {
	WHITESPACE: Symbol( "whitespace" ),
	KEYWORD: Symbol( "keyword" ),
	BINARY_COMPARISON_OPERATOR: Symbol( "binary_comparison_operator" ),
	BINARY_ARITHMETIC_OPERATOR: Symbol( "binary_arithmetic_operator" ),
	BINARY_LOGIC_OPERATOR: Symbol( "binary_logic_operator" ),
	UNARY_LOGIC_OPERATOR: Symbol( "unary_operator" ),
	DEREF_OPERATOR: Symbol( "deref_operator" ),
	PARENTHESIS: Symbol( "parenthesis" ),
	LITERAL_INTEGER: Symbol( "literal_integer" ),
	LITERAL_FLOAT: Symbol( "literal_float" ),
	LITERAL_STRING: Symbol( "literal_string" ),
	COMMA: Symbol( "comma" ),
} );
