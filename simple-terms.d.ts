declare namespace Cepharum.SimpleTerms {
    /**
     * Provides compiler for converting strings containing terms into
     * invocable functions suitable for evaluating those terms.
     */
    export class Compiler {
        /**
         * Compiles term in provided source returning function invocable for
         * evaluating the term in context of some data space.
         *
         * @param source string containing term to compile
         * @param functions pool of functions to support in term, omit for default
         * @param cache cache of previously compiled terms, provide for improved performance
         * @param variableQualifier callback invoked to map names addressing information in data space
		 * @param strictNaming if true, keywords naming properties of data to read from are kept as-is
		 * @returns function evaluating term on invocation
         */
        static compile( source: string, functions?: FunctionsPool, cache?: TermsCache, variableQualifier?: VariableQualifier, strictNaming?: boolean ): CompiledTerm;
    }

    /**
     * Provides interpolation of strings containing one ore more terms
     * embedded using special markers.
     */
    export class Interpolation {
        /**
         * Processes terms embedded in provided source replacing either
         * occurrence of a term with the result of its evaluation.
         *
         * @param source string assumed to contain embedded terms wrapped in `{{` and `}}`.
         * @param data data accessible on evaluating terms
         * @param functions functions invocable on evaluating terms, omit for default pool of functions
         * @param options interpolation customizations
         */
        static interpolate( source: string, data?: object, functions?: FunctionsPool, options?: InterpolationOptions ): string;

        /**
         * Parses provided source string for embedded terms providing parser
         * result for repeated evaluation.
         *
         * @param source string assumed to contain embedded terms wrapped in `{{` and `}}`.
         * @param functions functions invocable on evaluating terms, omit for default pool of functions
         * @param options interpolation customizations
         */
        static parse( source: string, functions?: FunctionsPool, options?: InterpolationOptions ): InterpolationParserResult;

        /**
         * Evaluates list of chunks as returned from @see parse().
         *
         * @param chunks
         * @param data
         */
        static evaluate( chunks: InterpolationParserChunk[], data?: object )
    }

    /**
     * Represents result of parsing string for embedded terms.
     */
    export type InterpolationParserResult = InterpolationParserChunk[] | CompiledTermFunction | string;

    /**
     * Represents single chunk which is either some literal string or some
     * term to process.
     */
    export type InterpolationParserChunk = CompiledTerm | string;

    /**
     * Defines available customizations for string interpolation.
     */
    export interface InterpolationOptions {
        /**
         * Provides string indicating start of embedded term. Defaults to `{{`.
         */
        opener?: string;

        /**
         * Provides string indicating end of embedded term. Defaults to `}}`.
         */
        closer?: string;

        /**
         * Provides cache of previously compiled terms to use for improved
         * performance.
         */
        cache?: TermsCache;

        /**
         * Provides callback to use for mapping names addressing data space
         * on compiling embedded terms.
         */
        qualifier?: VariableQualifier;

        /**
         * Indicates whether returning list of chunks to use with @see
         * Interpolation.evaluate() or a function wrapping that. Set true to
         * achieve the latter.
         */
        asFunction?: boolean;

        /**
         * Indicates whether returning single-chunk list or original string
         * in case of provided string does not contain any computable term.
         * Set true to achieve the latter.
         */
        keepLiteralString?: boolean;

		/**
		 * Controls whether keywords addressing data for read access are used as
		 * given in term (if `true`) or converted to lowercase letters (if
		 * omitted or `false`).
		 */
		strictNaming?: boolean;
    }

    /**
     * Exposes default pool of functions available for invocation in terms.
     *
     * Use it to include those defaults with your custom pool of functions.
     *
     * @example { ...Functions, cubic: x => x * x * x }
     */
    export const Functions: FunctionsPool;

    /**
     * Represents simple function accepting data space for evaluating some
     * term returning the term's result.
     */
    export type CompiledTermFunction = (data: object) => any;

    /**
     * Defines augmented function suitable for evaluating compiled term.
     */
    export interface CompiledTerm extends CompiledTermFunction {
        /**
         * Lists names used in term for accessing data space on term
         * evaluation.
         */
        dependsOn: string[];

        /**
         * Exposes function pool provided at compile time.
         */
        functions: FunctionsPool;
    }

    /**
     * Defines named pool of functions suitable for invoking in a term.
     */
    export interface FunctionsPool {
        [name: string]: Function;
    }

    /**
     * Maps strings previously provided for compiling contained term into
     * related compilation results with the latter depending on pool of
     * functions provided at compile time.
     *
     * Compiled terms are basically stateless thus suitable for re-use.
     */
    // @ts-ignore
    export type TermsCache = Map<string,CompiledTerm[]>;

    /**
     * Maps segments of a name addressing information in data space into
     * same or different name to actually used when accessing data space.
     *
     * Qualifiers are invoked at compile time when there is no access on
     * actual data space, thus this qualifier isn't capable of implementing
     * fallback behaviour in context of a particular data space.
     */
    export type VariableQualifier = (segments: string[]) => string[];
}
