export default {
	lang: "en",
	title: "simple-terms",
	outDir: "../public",
	base: "/simple-terms/",
	themeConfig: {
		nav: [
			{ text: "Home", link: "/" },
			{ text: "Topics", items: [
				{ text: "Installation", link: "/installation.md" },
				{ text: "Integration", link: "/integration.md" },
				{ text: "API reference", link: "/api.md" },
				{ text: "Term syntax", link: "/language.md" },
				{ text: "Term functions reference", link: "/functions" },
			] },
			{ text: "Code", link: "https://gitlab.com/cepharum-foss/simple-terms" },
			{ text: "Issues", link: "https://gitlab.com/cepharum-foss/simple-terms/-/issues" },
		],
		sidebar: "auto",
	}
};
