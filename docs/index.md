---
next: installation.md
---
# About

The _simple-terms_ package compiles a string containing a simple term into a Javascript function suitable for invoking with some data space for evaluating that term. 

It is maintaining **security** by actually parsing the provided string for complying with a limited syntax rather than using just `eval()`. Because of that _simple-terms_ can be used to significantly enhance use case processing user-provided data, e.g. on processing configuration files, custom data or l10n strings.

Term evaluation **performance** is high due to mapping accepted terms into code of Javascript functions to be created and returned. A compiled term becomes a Javascript function in the end. Because of that, lots of additional aspects such as order of processing operands in a logical or arithmetic expression depend on Javascript behaviour.

* [Installation](installation.md)  
  Learn how to install this package as part of your project.
  
* [Integration](integration.md)  
  See code examples for compiling and evaluating terms as well for interpolating strings containing terms.
  
* [API Reference](api.md)  
  Read detailed information on functions exposed by this package.
  
* [Language](language.md)  
  Learn details about the supported language.
  
* [Function Reference](functions.md)  
  Browse our reference manual for the pool of functions available in terms by default.
