---
next: integration.md
---
# Installation

Install this package by running 

```bash
npm install simple-terms
```

using CLI in your project's folder.
