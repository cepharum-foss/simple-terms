import { TokenTypes } from "./types.js";

/**
 * Lists supported operators consisting of two characters.
 *
 * @type {string[]}
 */
const DoubleCharOperators = [
	"==",
	">=",
	"<=",
	"<>",
	"&&",
	"||",
];

/**
 * Converts string of characters into string of tokens.
 */
export default class TermTokenizer {
	/**
	 * Exposes all types of tokens supported by tokenizer.
	 *
	 * @returns {object<string,Symbol>} list of supported token types
	 */
	static get Types() { return TokenTypes; }

	/**
	 * Converts provided string of characters into sequence of tokens.
	 *
	 * @param {string} code string of characters
	 * @param {boolean} omitWhitespace set true to exclude whitespace tokens from resulting sequence
	 * @returns {Token[]} resulting sequence of tokens
	 */
	static tokenizeString( code, omitWhitespace = false ) {
		const tokens = [];
		const length = code.length;
		let cursor = 0;
		let start = 0;
		let type = null;
		let newType = null;
		let previousChar = null;

		for ( ; cursor < length; cursor++ ) {
			newType = type;

			const currentChar = code.charAt( cursor );

			switch ( type ) {
				case TokenTypes.LITERAL_STRING :
					if ( currentChar === code.charAt( start ) ) {
						tokens.push( {
							type,
							value: code.substring( start, cursor + 1 ),
							offset: start,
						} );

						newType = type = null;
						start = cursor + 1;
					} else if ( currentChar === "\\" ) {
						cursor++;
					}
					break;

				default :
					switch ( currentChar ) {
						case " " :
						case "\r" :
						case "\n" :
						case "\t" :
						case "\f" :
							newType = TokenTypes.WHITESPACE;
							break;

						case "=" :
						case "<" :
						case ">" :
							newType = TokenTypes.BINARY_COMPARISON_OPERATOR;
							break;

						case "/" :
						case "*" :
						case "+" :
						case "-" :
							newType = TokenTypes.BINARY_ARITHMETIC_OPERATOR;
							break;

						case "&" :
						case "|" :
							newType = TokenTypes.BINARY_LOGIC_OPERATOR;
							break;

						case "!" :
							newType = TokenTypes.UNARY_LOGIC_OPERATOR;
							break;

						case "," :
							newType = TokenTypes.COMMA;
							break;

						case "." :
							switch ( type ) {
								case TokenTypes.LITERAL_INTEGER :
									newType = type = TokenTypes.LITERAL_FLOAT;
									break;

								case TokenTypes.BINARY_ARITHMETIC_OPERATOR :
									switch ( code.charAt( cursor - 1 ) ) {
										case "+" :
										case "-" :
											newType = type = TokenTypes.LITERAL_FLOAT;
											break;

										default :
											newType = TokenTypes.DEREF_OPERATOR;
									}
									break;

								default :
									newType = TokenTypes.DEREF_OPERATOR;
							}
							break;

						case "(" :
						case ")" :
							newType = TokenTypes.PARENTHESIS;
							break;

						case "0" :
						case "1" :
						case "2" :
						case "3" :
						case "4" :
						case "5" :
						case "6" :
						case "7" :
						case "8" :
						case "9" :
							switch ( type ) {
								case TokenTypes.BINARY_ARITHMETIC_OPERATOR :
									switch ( code.charAt( cursor - 1 ) ) {
										case "+" :
										case "-" :
											type = TokenTypes.LITERAL_INTEGER;
									}

									newType = TokenTypes.LITERAL_INTEGER;
									break;

								case TokenTypes.DEREF_OPERATOR :
									newType = type = TokenTypes.LITERAL_FLOAT;
									break;

								case TokenTypes.KEYWORD :
								case TokenTypes.LITERAL_FLOAT :
									break;

								default :
									newType = TokenTypes.LITERAL_INTEGER;
							}
							break;

						case "\"" :
						case "'" :
							newType = TokenTypes.LITERAL_STRING;
							break;

						default : {
							const cp = code.codePointAt( cursor );
							if ( ( cp >= 0x41 && cp <= 0x5a ) || ( cp >= 0x61 && cp <= 0x7a ) || cp === 0x5f || cp === 0x24 ) {
								// latin letters, underscore and dollar sign
								newType = TokenTypes.KEYWORD;
							}
						}
					}
			}

			if ( newType === type ) {
				// assumption: cursor > start

				switch ( newType ) {
					case TokenTypes.BINARY_COMPARISON_OPERATOR :
					case TokenTypes.BINARY_ARITHMETIC_OPERATOR :
					case TokenTypes.BINARY_LOGIC_OPERATOR : {
						// assumption: cursor == start + 1
						if ( DoubleCharOperators.indexOf( previousChar + currentChar ) > -1 ) {
							// consume double-character binary operator
							tokens.push( {
								type,
								value: code.substring( start, cursor + 1 ),
								offset: start,
							} );

							type = null;
							start = cursor + 1;
						} else {
							// consume non-combinable operator detected before
							tokens.push( {
								type,
								value: code.substring( start, cursor ),
								offset: start,
							} );

							start = cursor;
						}
						break;
					}
				}
			} else {
				// current character belongs to different type of token
				// -> consume any previously processed token
				if ( start !== cursor ) {
					if ( !omitWhitespace || type !== TokenTypes.WHITESPACE ) {
						tokens.push( {
							type,
							value: code.substring( start, cursor ),
							offset: start,
						} );
					}
				}

				switch ( newType ) {
					case TokenTypes.PARENTHESIS :
					case TokenTypes.UNARY_LOGIC_OPERATOR :
					case TokenTypes.COMMA :
						// instantly consume current token, too
						tokens.push( {
							type: newType,
							value: code.substr( cursor, 1 ),
							offset: cursor,
						} );

						start = cursor + 1;
						type = newType = null;
						break;

					default :
						start = cursor;
						type = newType;
				}
			}

			previousChar = currentChar;
		}

		if ( start !== cursor ) {
			if ( !omitWhitespace || type !== TokenTypes.WHITESPACE ) {
				tokens.push( {
					type,
					value: code.substring( start, cursor ),
					offset: start,
				} );
			}
		}

		return tokens;
	}
}


/**
 * @typedef {object} Token
 * @property {int} type type of token
 * @property {string} value value of token
 * @property {int} offset extracted token's index into source code
 */
