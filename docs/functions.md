---
prev: language.md
---
# Term Function Reference

This document provides a brief overview of functions available for invocation in terms, by default.

:::tip Case-Insensitive Function Naming
In this reference function names are provided with lowercase letters. In terms, the case of function names is ignored and thus it is okay to use uppercase letters there as well.  
:::

## Type Casting

### array()

**Signature:** `array( arg, arg, ... )`

Returns all provided arguments as a single list of values.

```javascript
array( 1, 2, "test" )    // yields list of 1, 2 and "test"
```

### asarray() <Badge text="v0.3.3+"></Badge>

**Signature:** `asarray( arg, keepNullish = false )`

If provided argument is an array, it is returned as-is. Otherwise, if argument is non-nullish, the function returns an array consisting of that argument, only. If argument is nullish, the returned array is empty unless second argument is set `true`.

```javascript
asarray( 0 )              // yields array( 0 )
asarray( array( 0 ) )     // yields array( 0 )
asarray( null )           // yields array()
asarray( array( null ) )  // yields array( null )
```

### boolean()

**Signature:** `boolean( arg, recursive = null )`

Casts provided argument to boolean value. `null`, `undefined`, `false`, `""`, `NaN` and `0` become false. All other values become true.

**Starting with v0.4.0,** this function takes an optional second argument. When set, a collection provided in first argument is converted recursively.

```javascript
boolean( array( 1, 0 ) )          // yields true
boolean( array( 1, 0 ), true )    // yields array( true, false )
```

### dict() <Badge text="v0.3.2+"></Badge>

**Signature:** `dict( name1, value1, name2, value2, ... )`

Returns dictionary (which is a regular Javascript object) with properties' names and values fed in pairs by provided arguments. On providing odd number of arguments, the last named property has value `undefined`.

### formatnumber()

**Signature:** `formatnumber( number, decimalSeparator = ".", thousandsSeparator = "", fraction = null, sign = false )`

Renders numeric value in `number` as string obeying certain constraints:

* String value of `decimalSeparator` is used as decimal separator.
* String value of `thousandsSeparator` is used as thousands separator.
* `fraction` is an optional integer controlling number of fractional digits to show.

    * Non-negative values enforce selected number of fractional digits.
    * Negative values limit number of fractional digits preventing trailing zeroes from being rendered.
    * Any other value including the _nullish_ default results in number represented with as many fractional digits as required for accuracy.

* `sign` is a boolean value controlling whether sign must be included with positive numbers, too.

### idict() <Badge text="v0.3.2+"></Badge>

**Signature:** `dict( name1, value1, name2, value2, ... )`

Returns dictionary (which is a regular Javascript object) with properties' names and values fed in pairs by provided arguments. Names are converted to lowercase. On providing odd number of arguments, the last named property has value `undefined`.

### integer()

**Signature:** `integer( arg, recursive = null )`

Parses provided argument as integer, thus returning leading sequence of digits optionally prefixed by some whitespace and a single sign.

**Starting with v0.4.0,** this function takes an optional second argument. When set, a collection provided in first argument is converted recursively.

```javascript
integer( array( "1", "5" ) )          // yields 1
integer( array( "1", "5" ), true )    // yields array( 1, 5 )
```

### number()

**Signature:** `number( arg, decimalSeparator = ".", thousandSeparator = ",", recursive = null )`

Parses provided argument as decimal obeying optionally provided decimal separator and thousands separator.

**Starting with v0.4.0,** this function takes an optional second argument. When set, a collection provided in first argument is converted recursively.

```javascript
number( array( "1.3", "5.0" ), ".", "," )          // yields 1.3
number( array( "1.3", "5.0" ), ".", ",", true )    // yields array( 1.3, 5 )
```

### string()

**Signature:** `string( arg, recursive = null )`

Returns string value of provided argument. If argument is nullish, empty string is returned. Apart from that semantics are identical to type-casting to string in Javascript.

```javascript
string( false )              // yields "false"
string( array( 1, 2, 3 ) )   // yields "1,2,3"
```

**Starting with v0.4.0,** a second argument can be set to request individual conversion of a collection's items.

```javascript
string( false, true )              // yields "false"
string( array( 1, 2, 3 ), true )   // yields array( "1", "2", "3" )
```

### uncollect() <Badge text="v0.4.0+"></Badge>

**Signature:** `uncollect( collection, name = null )`

Extracts first value from a provided collection or returns provided value as given if it's not a collection.

```javascript
uncollect( array( 5, 4, 3 ) )             // yields 5
uncollect( true )                         // yields true
```

Dictionaries are different: they may not have a clue which one of its properties is the "first" one to extract. Thus, you should provide the name of property assumed to be first one in second argument if provided collection is a dictionary. Without that, any property could be picked as first one.

```javascript
uncollect( dict( "a", 1, "b", 2 ) )             // might yield 1 or 2
uncollect( dict( "a", 1, "b", 2 ), "b" )        // always yields 2
```

## Strings

### centerpad()

**Signature:** `centerpad( arg, minSize, padding = " " )`

Returns provided argument as string repeatedly prepending and appending string in `padding` to ensure minimum length of resulting string as given in `minSize`.

### leftpad()

**Signature:** `leftpad( arg, minSize, padding = " " )`

Returns provided argument as string repeatedly prepending string in `padding` to ensure minimum length of resulting string as given in `minSize`.

### lowercase()

**Signature:** `lowercase( arg )`

Returns provided argument as string replacing all uppercase letters with their lowercase variants. This function is using `String#toLocaleLowerCase()`.

```javascript
lowercase( " ... Hello World! " )   // yields " ... hello world! "
```

**Starting with v0.3.1,** on providing a collection, either of its values is processed separately and the resulting set of converted values is returned as array. 

```javascript
lowercase( array( "Hello", "World", null ) )   // yields array( "hello", "world", null )
```

**Starting with v0.4.0,** the original structure of the provided collection is kept intact and `lowercase()` is recursively processing its string values, only. So, if you provide a dictionary of strings, the result of `lowercase()` is still a dictionary of strings. Any boolean, numeric or nullish value is kept as-is.

```javascript
lowercase( array( "Hello", "World", false ) )   // yields array( "hello", "world", false )
```

Function [`string()`](#string) has been extended accordingly to support a nested conversion of collection items into strings.

```javascript
lowercase( string( array( "Hello", "World", false ), true ) )   // yields array( "hello", "world", "false" )
```

### normalize()

**Signature:** `normalize( arg )`

Returns provided argument as string with any leading or trailing whitespace removed as well as consecutive sequences of arbitrary whitespace replaced with single occurrence of SPC (ASCII 32).

```javascript
normalize( " \n Hello  World! \t I  am John." )   // yields "Hello World! I am John."
```

### rightpad()

**Signature:** `rightpad( arg, minSize, padding = " " )`

Returns provided argument as string repeatedly appending string in `padding` to ensure minimum length of resulting string as given in `minSize`.

### slugify() <Badge text="v0.3.3+"></Badge>

**Signature:** `slugify( arg, snake_case = false )`

Computes [slug](https://en.wikipedia.org/wiki/Clean_URL#Slug) from provided argument as string. On providing a collection, either of its values is processed separately and the resulting set of converted values is returned as array. Nullish values are kept as-is.

A slug is computed in this way:

* Any non-string value is cast to string, first. _(This has changed in v0.4. See below!)_
* Any leading or trailing whitespace is stripped off.
* Any uppercase letter is prepended with a dash.
* Any leftover sequence of whitespace and/or special characters is replaced with a single dash.
* The result is converted to lower case letters.

```javascript
slugify( " ... Hello World! " )   // yields "-hello-world-"
```

The second optional argument can be set to use underscore instead of dash. This is helpful in case resulting names are required to work as keyword commonly valid in programming languages. 

```javascript
slugify( " ... Hello World! ", true )   // yields "_hello_world_"
```

**Starting with v0.4.0,** this function adjusts existing string values, only. Any other scalar input value is kept as-is. On providing a collection of values, its items are processed recursively and only existing string values are converted to slugs.

```javascript
slugify( array( "John Doe", false ) )   // yields array( "john-doe", false )
```

### substring() <Badge text="v0.3.3+"></Badge>

**Signature:** `substring( input, startAt = 0, endAt = Infinity )`

Expects a string in first argument and extracts a substring of its characters. If provided `input` is not a string, it gets converted to a string first. On providing nullish input, the result is `null`.

Characters of a string are addressed by numeric indices. Counting starts with 0. The second argument `startAt` is the index of first character to return. Third argument `endAt` provides index of character _following_ the last one to return.

Either index can be negative selecting characters off the end of provided string.

```javascript
substring( "Hi John!", 3, 7 )     // yields "John" 
substring( "Hi John!", -5, -1 )   // yields "John"
```

Second and third argument are optional. Omitting third one results in all characters following index in second argument being returned. If both arguments are omitted, the complete string is returned.

### trim()

**Signature:** `trim( arg )`

Returns provided argument as string with any leading or trailing whitespace removed.

### uppercase()

**Signature:** `uppercase( arg )`

Returns provided argument as string replacing all lowercase letters with their uppercase variants. This function is using `String#toLocaleUpperCase()`.

```javascript
uppercase( " ... Hello World! " )   // yields " ... HELLO WORLD! "
```

**Starting with v0.3.1,** on providing a collection, either of its values is processed separately and the resulting set of converted values is returned as array.

```javascript
uppercase( array( "hello", "world", null ) )  // yields array( "HELLO", "WORLD", null )
```

**Starting with v0.4.0,** the original structure of the provided collection is kept intact and `uppercase()` is recursively processing its string values, only. So, if you provide a dictionary of strings, the result of `uppercase()` is still a dictionary of strings. Any boolean, numeric or nullish value is kept as-is. 

```javascript
uppercase( array( "Hello", "World", false ) )   // yields array( "HELLO", "WORLD", false )
```

Function [`string()`](#string) has been extended accordingly to support a nested conversion of collection items into strings.

```javascript
uppercase( string( array( "Hello", "World", false ), true ) )   // yields array( "HELLO", "WORLD", "FALSE" )
```

## Numbers

### abs()

**Signature:** `abs( arg )`

Returns numeric absolute value of provided argument, thus dropping any sign.

### ceil()

**Signature:** `ceil( arg, precision = 0 )`

Returns numeric value of provided argument as float rounding up its value with given `precision` which is an integer. Positive values round to the selected number of digits following decimal separator. Negative values round to the selected number of digits preceding decimal separator.

### floor()

**Signature:** `floor( arg, precision = 0 )`

Returns numeric value of provided argument as float rounding down its value with given `precision` which is an integer. Positive values round to the selected number of digits following decimal separator. Negative values round to the selected number of digits preceding decimal separator.

### random()

**Signature:** `random( lower = 0, upper = 1000 )`

Returns random integer value in range from `lower` inclusively to `upper` exclusively.

### round()

**Signature:** `round( arg, precision = 0 )`

Returns numeric value of provided argument as float rounding its value with given `precision` which is an integer. Positive values round to the selected number of digits following decimal separator. Negative values round to the selected number of digits preceding decimal separator.

## Collections

### concat()

**Signature:** `concat( arg, arg, ... )`

Returns sole list of values resulting from concatenating all provided lists of values.

```javascript
concat( array( 1, 2 ), array( 3, 4 ) )   // yields array( 1, 2, 3, 4 )
```

### filter()

**Signature:** `filter( list )`

Returns copy of provided `list` of values removing all _falsy_ values.

```javascript
filter( array( 1, null, false, true ) )   // yields array( 1, true )
```

### indexof()

**Signature:** `indexof( haystack, needle, regexp = false, startAt = 0 )`

Traverses list of values provided as `haystack` for the first occurrence of value provided as `needle` returning that value's index into the list. If no value matches, -1 is returned.

By setting optional argument `regexp` the value in `needle` is considered a string describing regular expression to test against either value in list.

```javascript
indexof( array( "foo", "bar", "baz", "bam" ), "ba[mz]", true )   // yields 2
```

`haystack` must be a list of values. Otherwise, the result is always `-1`.

```javascript
indexof( array( 1, 2, 3, 2, 4 ), 2 )   // yields 1
```

Starting with version v.0.3.3, a fourth argument can be provided to pick a custom index to start searching at for closest succeeding match. If omitted, the search starts at first item of collection.

```javascript
indexof( array( 1, 2, 3, 2, 4 ), 2, false, 2 )   // yields 3
```

### item()

**Signature:** `item( list, index, fallback = null )`

Extracts single item from `list` of values selected by its 0-based `index` into the list. If list does not cover selected index, the optionally provided `fallback` is returned.

```javascript
item( array( "1", "2", "3", "2", "4" ), 2 )   // yields "3"
```

### join()

**Signature:** `join( list, glue = "" )`

Returns string resulting from concatenating all values of provided `list` using `glue` as separator.

```javascript
join( array( 1, "test", true ), "-" )   // yields "1-test-true"
```

### keys() <Badge text="v0.3.1+"></Badge>

**Signature:** `keys( collection )`

Returns the keys of a provided collection. If collection is an array or Set, a sequence of item indices is returned. If collection is an object or Map, the property names or keys are returned. Any other value results in an empty array.

```javascript
keys( dict( "foo", 1, "bar", 2 ) )   // yields array( "foo", "bar" )
```

### lastindexof() <Badge text="v0.3.3+"></Badge>

**Signature:** `lastindexof( haystack, needle, regexp = false, startAt = 999999999 )`

Traverses list of values provided as `haystack` for the last occurrence of value provided as `needle` returning that value's index into the list. If no value matches, -1 is returned.

By setting optional argument `regexp` the value in `needle` is considered a string describing regular expression to test against either value in list.

`haystack` must be a list of values. Otherwise, the result is always `-1`.

```javascript
lastindexof( array( 1, 2, 3, 2, 4 ), 2 )   // yields 3
```

A fourth argument can be provided to pick a custom index to start searching at for closest preceding match. If omitted, the search starts at last item of collection.

```javascript
lastindexof( array( 1, 2, 3, 2, 4 ), 2, false, 2 )   // yields 1
```

### length()

**Signature:** `length( arg )`

On providing an list of values in `arg` this function returns the number of items in that list. If `arg` is a string, the result is the number of characters in that string. The function returns `NaN` on providing any other type of value.

```javascript
length( array( 1, 2, 3, 2, 4 ) )   // yields 5
```

### lowerkeys() <Badge text="v0.3.2+"></Badge>

**Signature:** `lowerkeys( dictionary, recursive = false )`

The function returns copy of provided dictionary with keys converted to lowercase. If provided input is not a dictionary, the data is provided as-is except for collections which are returned as copies.

When providing truthy value in second argument nested dictionaries are processed, too.

### slice() <Badge text="v0.3.3+"></Badge>

**Signature:** `slice( input, startAt = 0, endAt = Infinity )`

Expects a collection in first argument and extracts a subset of its items. If first argument is a scalar, it gets wrapped in a collection consisting of that non-nullish scalar, only. On providing a dictionary, its values are used as collection.

Items are addressed by numeric indices. Counting starts with 0. The second argument `startAt` is the index of first item to return. Third argument `endAt` provides index of item _following_ the last one to return. 

Either index can be negative selecting items off the end of collection.

```javascript
slice( array( 1, 2, 3, 4, 5 ), 2, 4 )     // yields array( 3, 4 ) 
slice( array( 1, 2, 3, 4, 5 ), -3, -1 )   // yields array( 3, 4 )
```

Second and third argument are optional. Omitting third one results in all items following index in second argument being returned. If both arguments are omitted, the complete collection is returned.

### split()

**Signature:** `split( string, separator = "", regexp = false )`

Splits provided `string` into chunks separated from each other by provided `separator`. When omitting `separator` the function returns a list of characters in provided string.

```javascript
split( "hello" )        // yields array( "h", "e", "l", "l", "o" )
split( "hello", "l" )   // yields array( "he", "", "o" )
```

Optional parameter `regexp` may be set to indicate that `separator` is representing a regular expression instead of a fixed string.

```javascript
split( "foo-bar_baz", "[-_]", true )   // yields array( "foo", "bar", "baz" )
```

### spread() <Badge text="v0.3.1+"></Badge>

**Signature:** `spread( collection, strict = false )`

This function counts occurrences of values in a provided collection.

```javascript
spread( array( 1, 4, "4", 4 ) )   // yields dict( "1", 1, "4", 3 )
```

By default, all values are converted to string before counting. Setting second argument true, the values are counted as-is. In this case the resulting collection is a map instead of an object.

```javascript
spread( array( 1, 4, "4", 4 ), true )   // yields different counts on 4 and "4"
```

### unique() <Badge text="v0.3.1+"></Badge>

**Signature:** `unique( collection )`

The function returns provided collection with duplicate values stripped off. If provided value is not a collection but some scalar value, that value is returned as-is.

```javascript
unique( array( 1, 2, 3, 2, 4 ) )   // yields array( 1, 2, 3, 4 )
unique( 5 )                        // yields 5
unique( null )                     // yields null
```

**Prior to v0.4.0,** any scalar input has been returned as a single-item array consisting of that value. Function [`asarray()`](#asarray) has been introduced to achieve equivalent results.

## Date/Time

### dateadd()

**Signature:** `dateadd( timestamp, amount, unit = "s" )`

Returns provided `timestamp` adjusted by selected `amount`. Supported values for `unit` are identical to those listed on [datediff()](#datediff) except for the last two.

Resulting timestamp is given in number of seconds since [Unix Epoch](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Date#the_ecmascript_epoch_and_timestamps).

### datediff()

**Signature:** `datediff( first, second, unit = "s", absolute = false )`

Returns difference between the two provided timestamps each pre-processed using function [parsedate()](#parsedate) in selected `unit`. Supported units are:

| Value of `unit` | unit for returned difference |
|-----------------|------------------------------|
| `S` or `s` | seconds |
| `I` or `i` | minutes |
| `H` or `h` | hours |
| `D` or `d` | days |
| `W` or `w` | weeks |
| `M` or `m` | months |
| `Y` or `y` | years |
| `NM` or `nm` | months (non-linear) |
| `NY` or `ny` | years (non-linear)  |

Last two options obey different lengths of months and years.

Returned value is difference of `first` date/time from `second` date/time. If first date/time is earlier than second one, the resulting value is negative. By setting `absolute`, absolute values are returned preventing negative results.

### describedate()

**Signature:** `describedate( timestamp = null, utc = false )`

Splits date/time information given as timestamp in seconds since [Unix Epoch](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Date#the_ecmascript_epoch_and_timestamps) into its pieces and returns those as a dictionary.

By omitting timestamp, current date/time is used. When setting `utc` the resulting object's properties are representing given local time in UTC.

```javascript
describedate( 0 ).year   // yields 1970
```

The resulting dictionary consists of these properties:

| property name | value |
|---------------|-------|
| `year` | year, numeric, four digits |
| `month` | month, numeric, 1 is for January |
| `day` | day of month, numeric |
| `dow` | day of week, 0 is for Sunday |
| `hour` | hours into current day |
| `minute` | minutes into current how |
| `second` | seconds into current minute |

### droptime()

**Signature:** `droptime( timestamp )`

Returns provided number of seconds since [Unix Epoch](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Date#the_ecmascript_epoch_and_timestamps) without information on time of day, thus rounding value down start of selected day.

```javascript
describedate( droptime( now() ) ).hour     // yields 0
describedate( droptime( now() ) ).minute   // yields 0
describedate( droptime( now() ) ).second   // yields 0
```

### formatdate()

**Signature:** `formatdate( template, timestamp = null )`

Renders provided date/time information given as `timestamp` in seconds since [Unix Epoch](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Date#the_ecmascript_epoch_and_timestamps) as string according to provided `template`.

`template` is a string consisting of selected letters to be replaced with pieces of date/time information. Any other character is kept as-is. In addition, C-style escapes may be used to literally include those selected letters in resulting string.

| letter in template | replaced with |
|--------------------|---------------|
| `D` or `d` | day of month, padded with leading zero |
| `M` or `m` | 1-based index of month, padded with leading zero |
| `Y` or `y` | full year |
| `J` or `j` | day of month without any padding |
| `N` or `n` | 1-based index of month without any padding |
| `H` or `h` | hours of time of day, padded with leading zero |
| `I` or `i` | minutes of time of day, padded with leading zero |
| `S` or `s` | seconds of time of day, padded with leading zero |
| `G` or `g` | hours of time of day without any padding |

```javascript
formatdate( "y-m-d g:i", 0 )   // yields "1970-01-01 0:00"
```

### now()

**Signature:** `now( utc = false )`

Returns current number of seconds since [Unix Epoch](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Date#the_ecmascript_epoch_and_timestamps). By setting `utc` the returned value represents current time in UTC zone.

```javascript
now()   // yields a numeric value like 1653772808.974
```

### parsedate()

**Signature:** `parsedate( arg )`

Extracts date/time information from provided argument. It supports

* numeric values as decimal number or string assumed to represent seconds since [Unix Epoch](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Date#the_ecmascript_epoch_and_timestamps),

* a string representing date/time information in a format supported by Javascript's [Date.parse() function](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Date/parse) or

* an instanceof Javascript's `Date` class.

The function returns extracted date/time information as number of seconds since [Unix Epoch](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Date#the_ecmascript_epoch_and_timestamps) or `NaN` if extracting date/time information has failed.

```javascript
parsedate( "2022-02-01T13:12:54" )   // yields 1643717574
```

## Statistical Processing

### average()

**Signature:** `average( list )`

Returns average value of numeric values in provided `list`.

This function is obeying values that can be parsed as decimal number, only.

```javascript
average( array( 3, 8, 3, 2 ) )   // yields 4
```

### count()

**Signature:** `count( list )`

Returns number of numeric values in provided `list` of values.

This function is obeying values that can be parsed as decimal number, only.

```javascript
count( array( 3, 8, 3, 2 ) )   // yields 4
```

### max()

**Signature:** `max( arg, arg, ... )`

Returns argument with the biggest numeric value.

**Signature:** `max( list )`

Returns the biggest numeric value in provided `list` of values.

```javascript
max( array( 3, 8, 3, 2 ) )   // yields 8
```

### median()

**Signature:** `median( list )`

Returns median value of numeric values in provided `list`.

This function is obeying values that can be parsed as decimal number, only.

```javascript
median( array( 3, 8, 3, 2 ) )   // yields 3
```

### min()

**Signature:** `min( arg, arg, ... )`

Returns argument with the smallest numeric value.

```javascript
min( array( 3, 8, 3, 2 ) )   // yields 2
```

**Signature:** `min( list )`

Returns the smallest numeric value in provided `list` of values.

### sum()

**Signature:** `sum( list )`

Returns sum of all numeric values in provided `list` of values.

```javascript
sum( array( 3, 8, 3, 2 ) )   // yields 16
```

## Conditionals

### first()

**Signature:** `first( arg, arg, ... )`

Returns value of first argument in list of provided arguments which has a non-nullish value. This includes some falsy values such as `false` or the empty string.

```javascript
first( null, undefined, 1 )          // yields 1
first( null, undefined, false, 1 )   // yields false
```

### isset()

**Signature:** `isset( arg, arg, ... )`

Returns `true` if one of the arguments isn't _nullish_.

```javascript
isset( null, undefined )          // yields false
isset( null, undefined, false )   // yields true
```

### test()

**Signature:** `test( condition, positive, negative = null )`

Returns value of argument `positive` if condition is truthy. Otherwise, value of argument `negative` is returned.

```javascript
test( length( array( 1, 2, 3, 4 ) ) > 3, "many", "few" )   // yields "many"
test( length( array( 1, 2 ) ) > 3, "many", "few" )         // yields "few"
```

:::tip  
Use this function to implement conditional processing similar to **if**-statement or ternary operator in regular programming languages.
:::

### type() <Badge text="v0.3.3+"></Badge>

**Signature:** `type( arg )`

Delivers type of provided argument. Returned value is a string. Supported values are:

* `"null"` if argument is nullish (`null` or `undefined`)
* `"boolean"` if argument is `true` or `false`
* `"number"` if argument is a numeric value (in opposition to a string representing a numeric value)
* `"string"` if argument is a string
* `"array"` if argument is an array
* `"dict"` if argument is a dictionary
* `"unknown"` if argument is anything else.

```javascript
type( 3 )                     // yields "number"
type( "3" )                   // yields "string"
type( array( 3, 8, 3, 2 ) )   // yields "array"
type( describedate( 0 ) )     // yields "dict"
```

## Context Helper

### cookie()

**Signature:** `cookie( name )`

Returns value of named cookie in context of current browser runtime. It is returning `null`, if cookie is missing.

This method is accessing `document.cookie` which is usually available browser engines, only.

**Signature:** `cookie( name, true )`

Checks if named cookie has been set in context of current browser runtime returning the result as a boolean value. 

This method is accessing `document.cookie` which is usually available browser engines, only,
