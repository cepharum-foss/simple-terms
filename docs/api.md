---
prev: integration.md
next: language.md
---
# API Reference

The package exposes the following elements:

## Compiler

```javascript
import { Compiler } from "simple-terms";
```

### Compiler.compile()

**Signature:** `Compiler.compile( string, functions, cache, qualifier, strictNaming )`

Converts a term in `string` into a function suitable for evaluating that term by invocation. 

Optional extra arguments are:

  * `functions: object<string, function>` is a pool of functions supported in a term. It is used to validate the term and will throw if term is trying to invoke a function that isn't listed here.

  * `cache: Map` may refer to a collection of previously compiled terms. It is used to improve performance of compilation by looking up a term in cache and returning previous compilation result on hit. It will be populated with compilation result otherwise.

  * `qualifier( string[] ): string[]` is a callback invoked to qualify variable names found in terms. It is mostly useful on implementing aliases e.g. to have shorter names using special variables in a complex data space.

    In terms, data is read by naming properties. Data space is organized hierarchically. Thus, names are supporting dot-notation syntax for descending into data space. Names provided in dot-notation are split into list of segments separated by the dots. This function is invoked with the segments of a name found in the term. It is expected to return another list of segments to actually use for reading the data.

    ```javascript
    const q = segments => {
        if ( segments[0] === "$special" ) {
            return ["special", "backend", "meta"].concat( segments.slice( 1 ) );
        }
          
        return ["local", "contextX"].concat( segments );
    };
      
    const fn = Compiler.compile( "$special.major + minor", null, null, q );
      
    console.log( fn( {
        special: { backend: { meta: { major: 3 } } },
        local: { contextX: { minor: 5 } },
    } ) ); // showing: 8
    ```

  * `strictNaming: boolean` <Badge text="0.4.2+" type="info"/> controls whether keywords addressing data for read access are used as given in term (if `true`) or converted to lowercase letters (if omitted or `false`).

     By default, all keywords are converted to lowercase. This applies
  
     * to those representing literal values (`true`, `false` or `null`), 
     * to names of functions to invoke and 
     * to names of variables to read on evaluating a term. 
 
    On setting this option, that last category of keywords is kept as-is to support accessing data with mixed-case property names.

    ```javascript
    const sloppy = Compiler.compile( "firstName + ' ' + lastName" );
    const strict = Compiler.compile( "firstName + ' ' + lastName", null, null, null, true );
    
    const mixedCase =  { firstName: "John", lastName: "Doe" };
    const lowerCase =  { firstname: "John", lastname: "Doe" };
      
    console.log( sloppy( mixedCase ) ); // showing: undefined undefined
    console.log( sloppy( lowerCase ) ); // showing: John Doe

    console.log( strict( mixedCase ) ); // showing: John Doe
    console.log( strict( lowerCase ) ); // showing: undefined undefined
    ```

#### Compilation Result

`Compiler.compile()` is returning a regular Javascript function. When invoked, the term gets evaluated returning its result. The function accepts a data space for term evaluation in its sole argument `data`.

In addition, there is a list of variables read by the term in property `dependsOn`:

```javascript
const fn = Compiler.compile( "$special.major + minor", null, null, q );
  
console.log( fn.dependsOn ); // shows array containing "$special.major" and "minor"
```

This information is useful in complex data processing applications that need to identify relationships between terms and their sources of information.

## Interpolation

```javascript
import { Interpolation } from "simple-terms";
```

### Interpolation.interpolate()

**Signature:** `Interpolation.interpolate( string, data, functions, options )`

Searches provided string for embedded terms. Every such term is compiled and evaluated in context of provided data space and pool of functions. It returns the resulting string.

This function is a convenience wrapper for parsing and evaluating term in a single step. Thus, `function` and `options` are forwarded to `parse()` and `data` is forwarded to `evaluate()`.

### Interpolation.parse()

**Signature:** `Interpolation.parse( string, functions, options )`

Searches provided `string` for embedded terms compiling every occurrence.

Argument `functions` is providing a custom pool of supported functions. See the [section on custom functions](integration.md#custom-functions) for additional information.

Depending on provided options this method returns a list of chunks, a function ready for evaluation or the provided string when there is no embedded term.

#### Supported Options

  * `asFunction : boolean` controls if `parse()` is returning an evaluation function ready for invoking with data space.

  * `keepLiteralString : boolean` controls if `parse()` is returning provided string as-is unless there is an embedded term.

  * `opener: string` and `closer: string` are defining strings used to indicate start and end of embedded terms. Their defaults are <code v-pre>{{</code> and `}}`.

    ```javascript
    import { Interpolation } from "simple-terms";
      
    const string = "The result is <..< max( outer.inner, 3 ) * 4 >..>!";
    const result = Interpolation.interpolate( string, {
        outer: {
            inner: 6
        },
    }, null, {
        opener: "<..<",       
        closer: ">..>",       
    } );
      
    console.log( result ); // showing "The result is 24!"
    ```

  * `cache: Map` may provide an instance of `Map` used for improving performance on compiling terms by re-using result of previous compilations when the source is identical.

  * `qualifier( string[] ): string[]` is an optional callback invoked with segments of a variable's name found in the term. The callback is assumed to return another sequence of segments addressing the information to look up in `data` on evaluating eventually.

    This option enables your code to e.g. implement aliases. Terms may address variables using dot-notation syntax. Provided segments result from splitting that address by the dots.

    See [Compiler.compile()](#compiler-compile) above for additional information on this callback.

* `strictNaming: boolean` <Badge text="0.4.2+" type="info"/> controls whether keywords addressing data for read access are used as given in term (if `true`) or converted to lowercase letters (if omitted or `false`).

  See [Compiler.compile()](#compiler-compile) above for additional information on this option.


### Interpolation.evaluate()

**Signature:** `Interpolation.evaluate( chunks, data )`

Evaluates sequence of chunks returned by `parse()` in context of data space provided in `data` returning the resulting string.

## Functions

```javascript
import { Functions } from "simple-terms";
```

This is the frozen pool of default functions. See the [function reference](functions.md) for additional information.
